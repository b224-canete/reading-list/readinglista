console.log("Hello World");
console.log(" ");

let word=`webmaster`;

function sortWord(letters) {
	return word.split("").sort().join("");
};

console.log(word);
console.log(sortWord(word));

console.log(" ");

let input = `The quick brown fox`;

function countVowels(param) {
	let count=0;
		for(let i=0; i < param.length; i++){
			if (
				param[i].toLowerCase() === "a" ||
				param[i].toLowerCase() === "e" || 
			 	param[i].toLowerCase() === "i" || 
			 	param[i].toLowerCase() === "o" || 
			 	param[i].toLowerCase() === "u"
			) {
				count++;
			};
		};
	return count;	
};

console.log(input);
console.log(countVowels(input));

console.log(" ");

let countries = ["Philippines", "Indonesia", "Vietnam", "Thailand"];

function addCountry(country){
	console.log(countries);
	countries.unshift(country);
	console.log(countries);
	countries.sort();
	console.log(countries);
};

addCountry("Malaysia");

console.log(" ");

let person = {
	"firstName": "Lhemar",
	"lastName": "Canete",
	"age": 26,
	"gender": "Male",
	"nationality": "Filipino"
};

console.log(person);

console.log(" ");

function People(firstName,lastName,age,gender,nationality) {
	this.firstName = firstName,
	this.lastName = lastName,
	this.age =age,
	this.gender = gender,
	this.nationality = nationality,
	this.talk = function(){
					console.log(`Hi, I am ${firstName} ${lastName}, ${age} years of age. I am a ${gender} ${nationality}.`)
				}
};

let friend1 = new People("Dee Spencer", "Watters", 44, "Male", "American");
console.log(friend1);
friend1.talk();
console.log(" ");

let friend2 = new People("Ray Fregee", "Raytor", 30, "Male", "German");
console.log(friend2);
friend2.talk();
console.log(" ");

let friend3 = new People("Pa Mhen", "Ta", 22, "Female", "Chinese");
console.log(friend3);
friend3.talk();